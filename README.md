# Halmak Keyboard Layout

This is an AI designed keyboard layout that was built within the
[keyboard-genetics] project.
More details and a documented process can be found on
the blog *[Nikolay Rocks]*.

![Keyboard Layout](assets/halmak-1471px.png)

## Layout variants

This repository features 3 additional layouts for Linux.

* Halmak for programmers (numbers-symbols row inverted)
* Halmak for programmers with H and L permuted
* Halmak, international variant with AltGr dead keys
```
┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
│ ~ ~ │ ! ¹ │ @ ˝ │ # ¯ │ $ £ │ % $ │ ^ ^ │ & ̛ │ * ˛ │ < ˘ │ > ° │ _  ̣ │ + ÷ ┃Backspace┃
│ ` ` │ 1 ¡ │ 2 ² │ 3 ³ │ 4 € │ 5 € │ 6 ¼ │ 7 ½ │ 8 ¾ │ 9 ‘ │ 0 ’ │ - ¥ │ = × ┃ ⌫       ┃
┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┳━━━━━━━┫
┃Tab    ┃ W Å │ L Ø │ R ® │ B B │ Z Æ │ : ° │ Q Ä │ U Ú │ D Ð │ J J │ { “ │ } ” ┃ ⏎     ┃
┃ ↹     ┃ w å │ l ø │ r ® │ b b │ z æ │ ; ¶ │ q ä │ u ú │ d ð │ j j │ [ « │ ] » ┃ Enter ┃
┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓      ┃
┃Caps    ┃ S § │ H H │ N Ñ │ T Þ │ ( Ç │ )  ̣ │ A Á │ E É │ O Ó │ I Í │ " " │ | ¦ ┃      ┃
┃Lock  ⇬ ┃ s ß │ h h │ n ñ │ t þ │ , ç │ . ˙ │ a á │ e é │ o ó │ i í │ ' ' │ \ ¬ ┃      ┃
┣━━━━━━┳━┹───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┲━┷━━━━━┻━━━━━━┫
┃Shift ┃ | > │ F F │ M µ │ V V │ C ¢ │ ?  ̉ │ G G │ P Ö │ X X │ K Œ │ Y Ü ┃Shift         ┃
┃ ⇧    ┃ \ < │ f f │ m µ │ v v │ c © │ / ¿ │ g g │ p ö │ x x │ k œ │ y ü ┃ ⇧            ┃
┣━━━━━━┻┳━━━━┷━━┳━━┷━━━━┱┴─────┴─────┴─────┴─────┴─────┴────┲┷━━━━━╈━━━━━┻┳━━━━━━━━━━━━━┛
┃Ctrl   ┃ Fn    ┃ Alt   ┃ ␣ Space                           ┃AltGr ┃ Ctrl ┃
┃       ┃       ┃       ┃                                   ┃      ┃ Menu ┃
┗━━━━━━━┻━━━━━━━┻━━━━━━━┹───────────────────────────────────┺━━━━━━┻━━━━━━┛
```

### Note about the H-L permutation

The second variant permutes the H-L-M keys so that L is supposed to
be more frequent than H, and H more frequent than M.

The letter H is [heavily used](http://norvig.com/mayzner.html) in the English language.
I (@flaur) personally do not write as much natural English language as
programming languages (and Vim keybindings),
and the latter tend to exhibit quite different character counts.

I do not have data to support the claim H is less used in code than
in natural English, but I feel confident in stating the letter H
infrequently appears in reserved keywords such as
`module`, `import`, `class`, `define`, `local`, `function`, `macro`,
`begin`, `end`, `if`, `for`, etc.
or typical datatypes such as
`type`, `integer`, `float`, `boolean`, `string`, `list`, `array`, etc.

As shown in diagram and trigram frequencies, H most often appears
in TH, HE, THE, etc. probably in words such as *the* or *that*.
Meanwhile, article words and prepositions are often omitted in variable
names and docstrings, to the point
I even believe *with* or *which* are more frequent than *the* or *that*.
An exception I can think of is `this`, but this keyword is specific
to a few programming languages I do not use.

I also write in several Latin languages that make heavy use of L.
Again, in these languages L most often appears in article words,
but I use these languages for natural language text only.
That is why I gave L higher priority than H.

I would be very much interested in any data regarding international
English, not to be confused with American English or British English.
I have always been under the impression that non-native English authors
also tend to omit the article *the* in places where native English speakers
would use it.

I also tried to invert M and H, so that H is even lower priority, but
H still is very frequent, overall.
I have just begun to log my key presses in an attempt to get more
objective data, although of course this will apply to my workflow.

> The reminder of this document applies to the original Halmak layout.

## Features

* Build based on the real world hand movements analysis
* Nearly maximal possible typing efficiency
* Very low overall fingers movement distance
* Very low same finger / same hand usage overheads
* Very low overall horizontal hands movement
* Highly symmetrical design that accounts for individual fingers strength
* Designed with the modern, web based English in mind

## Comparisons

During the research I've identified the following results: in terms of efficiency

* `QWERTY` - `0%` (baseline)
* `Dvorak` - `+77%`
* `Colemak` - `+84%`
* `Workman` - `+101%`
* `Halmak` - `+134%`

Please refer to the article *[Halmak Reborn]*
for the more detailed breakdown of the results.

All scripts are opensourced and can be verified in the
[keyboard-genetics] repository.

## The Name

The name is a combination of `HAL-9000`, as a reference to the layout being
designed by an AI. And, `Dvorak` as a gratitude to Mr. Dvorak for his dedication
to the layouts optimizations process. The letter `m` in between is just to make
it sound nicer. Or is it!?...

## Installation

* Git clone all the stuff somewhere
* Copy `Halmak.bundle` into your `/Library/Keyboard Layouts` folder (create if it is missing)
* Sign out, Sign in
* Go to the keyboard preferences and add the `Halmak` layout
* Ask here in issues if nothing works

### Android installation for physical keyboards

1. Install [Extra Physical Keyboard Layouts from the Play Store](https://play.google.com/store/apps/details?id=varzan.extraKeyboardLayouts)
Source code is available [on github](https://github.com/varzan/extra-keyboard-layouts)
1. Connect a physical keyboard to your Android device e.g. via bluetooth or OTG
1. Select the Halmak layout in your system menu e.g. System > Language and Input > Physical keyboard > *Keyboard Name*

## Integrations

### Kinesis Advantage 2

To enable native key mapping for Halmak on your [Kinesis Advantage 2](https://kinesis-ergo.com/shop/advantage2/)

1. [Download our `1_qwerty.txt` file](https://raw.githubusercontent.com/MadRabbit/halmak/master/integrations/kinesis/1_qwerty.txt)
1. <kbd>Program</kbd>+<kbd>Shift</kbd>+<kbd>Escape</kbd> to enable power user mode (should see 4 LED flashes)
1. <kbd>Program</kbd>+<kbd>F1</kbd> to mount to the keyboard as a drive (should see a few flashes)
1. Drag the downloaded `1_qwerty.txt` file into `ADVANTAGE2` / `active`
1. Open Disk Utility, right-click on the drive, select Eject (must be "Eject", as unmount will not write the file)
1. <kbd>Program</kbd>+<kbd>F3</kbd> to enable QWERTY, then <kbd>Program</kbd>+<kbd>1</kbd> to enable our Halmak modification of QWERTY

Refer to the [Kinesis Advantage 2 support page] for assistance.

### Type Fu

To learn Halmak, we have an integration for [Type Fu].

1. [Download our `Halmak.tfl` file](https://raw.githubusercontent.com/MadRabbit/halmak/master/integrations/typefu/Halmak.tfl)
1. Open Type Fu
1. Preferences
1. Keyboard
1. Hamburger menu
1. Import from File
1. Select the file you downloaded from the first step

## Copyright & License

Again, I'm not sure if there is a point to this. But I guess it's MIT.

Copyright (C) 2016 Nikolay Nemshilov

[keyboard-genetics]: https://github.com/MadRabbit/keyboard-genetics
[Halmak Reborn]: http://nikolay.rocks/2016-12-20-the-halmak-reborn

[Kinesis Advantage 2 support page]: https://kinesis-ergo.com/support/advantage2/

[Type Fu]: http://type-fu.com

[Nikolay Rocks]: http://nikolay.rocks/categories/optimal+keyboard

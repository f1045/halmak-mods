# Installation

Simply type:

``
sudo make install
``

## Uninstall

``
sudo make uninstall
``

If you already uninstalled and try to install the keymap again,
you may need to call make with the `-B` flag to force -update the symbols:

``
sudo make -B install
``

## Post-installation steps for Gnome

After running `make install`, you can restart Gnome with `Alt + F2` to get
a prompt and simply type `r`.
Under *Settings* > *Region & Language* > *Input Sources* > *+* > *English (United States)*,
you should find a several new entries including *English (Halmak)*.

# Original installation procedure

> The reminder of this document are MadRabbit's instructions.
> I (@flaur) did not even try the procedure described below.
> I guess my use cases are too basic to eventually run into
> any such overlay issue.

So it's a bit of a mess to make the QWERTY control overlay work in xkb.
To make it work I had to define a custom key type, six layers, three groups

layer1 and layer2 - the querty modifiers layer (ctrl,alt,super)
layer3 and layer4 - the actual Halmak keyboard
layer5 and layer6 - mac style special symbols for the ralt switch

It kind of works system wide, but not everywhere, some electron apps
are fucked. I have no other solution for VS Code other than manually
remap key bindings because it reads keycodes directly off the chrome
DOM events and messes it up.

Also, apparently xkb gets broken now and then. I've tested this on Ubnutu 20.04,
and have no guarantee that it will keep working. Fucking programmers, man...

## Installation

Copy the `symbols` and `types` folders into `/usr/share/X11/xkb`.
You'll have to overwrite the `types/complete` to make this work.

After that add `halmak` into the `rules/evdev.xml` wherever you
want this layout.

Restart.

## Alternate Install ( without qwerty layer )

This is an alternate install that doesn't add the qwerty control overlay. To install, run:

```
sudo cp symbols/halmak_no_qwerty /usr/share/X11/xkb/symbols/
setxkbmap halmak_no_qwerty
```
